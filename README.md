# System Config

## Install packages

On a Mac:

```sh
./macos/install
```

## Configure dotfiles

Taken from [here](https://www.atlassian.com/git/tutorials/dotfiles).

```sh
git clone --bare git@github.com:wpj/dotfiles.git $HOME/.cfg
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
config checkout
config config --local status.showUntrackedFiles no
```

To interact with this git repo, instead of using `git`, use the `config` alias.
